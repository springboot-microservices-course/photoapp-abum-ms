FROM openjdk:11-jdk-slim
VOLUME /tmp
COPY target/PhotoAppApiAlbums-0.0.1-SNAPSHOT.jar PhotoAppApiAlbums.jar
ENTRYPOINT ["java", "-jar", "PhotoAppApiAlbums.jar"]
